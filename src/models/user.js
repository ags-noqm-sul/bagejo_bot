'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  user.init({
    tele_id: DataTypes.INTEGER,
    username: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    access: DataTypes.STRING,
    last_access:DataTypes.STRING,
    active: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'user',
    timestamps: false
  });
  return user;
};