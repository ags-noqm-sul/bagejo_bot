'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transport extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  transport.init({
    ID_SITE: DataTypes.STRING,
    DATEL: DataTypes.STRING,
    STO: DataTypes.STRING,
    NAME_SITE: DataTypes.STRING,
    NAME_METRO: DataTypes.STRING,
    NAME_GPON: DataTypes.STRING,
    PORT_GPON: DataTypes.STRING,
    TIPE_ONT: DataTypes.STRING,
    SN_ONT: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'transport',
    timestamps: false
  });
  return transport;
};