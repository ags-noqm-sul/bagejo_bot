'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class desiqa extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  desiqa.init({
    SITEID: DataTypes.STRING,
    NEID: DataTypes.STRING,
    NEID_Sec: DataTypes.STRING,
    Site_Location: DataTypes.STRING,
    BTS_Name: DataTypes.STRING,
    Network: DataTypes.STRING,
    Vendor: DataTypes.STRING,
    LACCI: DataTypes.STRING,
    BSC_RNC_Id: DataTypes.STRING,
    BSC_RNC_name: DataTypes.STRING,
    BTS_no: DataTypes.STRING,
    BTS_Name_OSS: DataTypes.STRING,
    Cell_Name_OSS: DataTypes.STRING,
    Cell_Name_OSS_2: DataTypes.STRING,
    Site: DataTypes.STRING,
    BTS: DataTypes.STRING,
    Type: DataTypes.STRING,
    Sec_no: DataTypes.STRING,
    Sec_id: DataTypes.STRING,
    LAC: DataTypes.STRING,
    CI: DataTypes.STRING,
    Band: DataTypes.STRING,
    Bandwith_LTE: DataTypes.STRING,
    TRX: DataTypes.STRING,
    BTS_Type: DataTypes.STRING,
    PCI_SCR_BCCH: DataTypes.STRING,
    NCC: DataTypes.STRING,
    BCC: DataTypes.STRING,
    CGI: DataTypes.STRING,
    Remark_DB: DataTypes.STRING,
    Site_Long: DataTypes.STRING,
    Site_Lat: DataTypes.STRING,
    Azimuth: DataTypes.STRING,
    Cluster_Sales: DataTypes.STRING,
    NSA: DataTypes.STRING,
    RTPO: DataTypes.STRING,
    Branch: DataTypes.STRING,
    Sub_Branch: DataTypes.STRING,
    MSS_MGW: DataTypes.STRING,
    Micro_Macro: DataTypes.STRING,
    Type_Site: DataTypes.STRING,
    Power_Type: DataTypes.STRING,
    Daya: DataTypes.STRING,
    Transport_Type: DataTypes.STRING,
    Bandwith: DataTypes.STRING,
    Border_WBB: DataTypes.STRING,
    Tinggi_Tower: DataTypes.STRING,
    Tinggi_Antena: DataTypes.STRING,
    Remark_Tinggi_Antena: DataTypes.STRING,
    Tipe_Antena: DataTypes.STRING,
    Tilting_Antena: DataTypes.STRING,
    Last_Update_Antena: DataTypes.STRING,
    OA_Date: DataTypes.STRING,
    QC_Pass_Date: DataTypes.STRING,
    Propinsi: DataTypes.STRING,
    Kota_Kabupaten: DataTypes.STRING,
    Kecamatan: DataTypes.STRING,
    Kelurahan: DataTypes.STRING,
    Alamat: DataTypes.TEXT,
    Tower_Provider: DataTypes.STRING,
    Last_Update: DataTypes.STRING,
    Keterangan: DataTypes.STRING,
    Unvalidate_Date: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'desiqa',
    timestamps: false
  });
  return desiqa;
};