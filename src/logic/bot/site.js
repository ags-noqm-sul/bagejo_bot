const { desiqa, Sequelize, transport, revenue } = require("../../models/index")
const { toSiteString, toSiteList, toText } = require("../../helper/general.telegram.helper")

class SiteController {

    async findSiteId(msg) {
        try {
            let siteId = msg.text.split(" ")[1]
            let site = await desiqa.findOne(
                {
                    where:
                    {
                        "SITEID": siteId.toUpperCase()
                    },
                    attributes: ['SITEID', 'Site_Location', 'Alamat', 'Kelurahan', 'Kecamatan', 'Kota_Kabupaten', 'Propinsi', 'Site_Lat', 'Site_Long']
                }
            )

            let ret = (site) ? (`site finder ${siteId} result : \n\n` + toSiteString(site)) : `site tidak ditemukan`
            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }

    async findLacci(msg) {
        try {
            let LACCI = msg.text.split(" ")[1]
            let site = await desiqa.findOne(
                {
                    where:
                    {
                        "LACCI": LACCI.toUpperCase()
                    },
                    attributes: ['SITEID', 'Site_Location', 'Alamat', 'Kelurahan', 'Kecamatan', 'Kota_Kabupaten', 'Propinsi', 'Site_Lat', 'Site_Long']
                }
            )

            let ret = (site) ? (`lacci finder ${LACCI} result : \n\n` + toSiteString(site)) : `site tidak ditemukan`
            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }

    async enb(msg) {
        try {
            let enbid = msg.text.split(" ")[1]
            let site = await desiqa.findOne(
                {
                    where:
                    {
                        "BTS_no": enbid.toUpperCase()
                    },
                    attributes: ['SITEID', 'Site_Location', 'Alamat', 'Kelurahan', 'Kecamatan', 'Kota_Kabupaten', 'Propinsi', 'Site_Lat', 'Site_Long']
                }
            )

            let ret = (site) ? (`enb finder ${enbid} result : \n\n` + toSiteString(site)) : `site tidak ditemukan`
            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }

    async search(msg) {
        try {
            let keyword = msg.text.replace(msg.text.split(" ")[0] + " ", "")

            console.log(keyword)

            let sites = await desiqa.findAll(
                {
                    where: {
                        "Site_Location": {
                            [Sequelize.Op.like]: `%${keyword}%`
                        }
                    },
                    attributes: [Sequelize.fn('DISTINCT', 'SITEID'), 'SITEID', 'Site_Location'],
                    limit: 50,
                }
            )

            let ret = (sites) ? (`site search ${keyword} result : \n\n` + toSiteList(sites)) : `site tidak ditemukan`
            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }

    async trm(msg) {
        try {
            let siteid = msg.text.split(" ")[1]

            let trm = await transport.findOne(
                {
                    where:
                    {
                        "ID_SITE": siteid.toUpperCase()
                    }
                }
            )

            let ret
            if (trm) {
                let frmtrm = trm.dataValues
                delete frmtrm.id

                ret = `transport search ${siteid} result : \n\n` + toText(frmtrm)
            } else ret = 'bukan site metro'

            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }

    async revenue(msg) {
        try {
            let siteid = msg.text.split(" ")[1]

            let rev = await revenue.findOne(
                {
                    where:
                    {
                        "site_id": siteid.toUpperCase()
                    }
                }
            )

            let ret
            if (rev) {
                let frmtrm = rev.dataValues
                delete frmtrm.id

                ret = `revenue search ${siteid} result : \n\n` + toText(frmtrm)
            } else ret = 'site tidak ditemukan'

            return [{ text: ret }]

        } catch (error) {
            console.error(error)
        }
    }
}

module.exports = new SiteController()