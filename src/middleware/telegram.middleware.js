const telegramApi = require("../api/telegram");
const auth = require("../helper/auth.helper")
const CallbackHelper = require("../helper/callback.helper")
const homecontroller = require("../logic/bot/home")
const homeController = new homecontroller()
const siteController = require("../logic/bot/site")
const { callBackTemplate } = require("../helper/general.telegram.helper")

class TelegramMiddleware {
    async run(req, res, next) {
        try {
            const message = req.body.message

            if(message.chat.type === 'group'){
                await telegramApi.send(message.chat.id, { text: "group not implemented yet" },0)

                next()
                return
            }

            console.log("capture request==================",message)

            if (req.body.callback_query) {
                let msgc = req.body.callback_query
                let chatId = msgc.message.chat.id
                let clbkId = msgc.id
                let key = msgc.data

                let clbkMessage = callBackTemplate(key)

                await telegramApi.send(chatId, { text: clbkMessage },0)
                await telegramApi.sendCallback(clbkId)

                next()
                return
            }
            
            let logId = await auth.logger(message)
            const keyuser = message.text.split(" ")[0].toLowerCase()

            if (! await auth.validate(message)) {
                await telegramApi.send(message.chat.id, {
                    text: "Maaf user anda tidak terdaftar di bot ini, silahkan hubungi @agsprg untuk info lebih lanjut"
                },logId)

                next()
                return
            }

            let keyword = [
                ['/start', homeController.welcome],
                ['site', siteController.findSiteId],
                ['revenue', siteController.revenue],
                ['lacci', siteController.findLacci],
                ['enb', siteController.enb],
                ['trm', siteController.trm],
                ['search', siteController.search]
            ]

            for (const key in keyword) {

                if (keyuser === keyword[key][0]) {
                    let msg = await keyword[key][1](message)

                    for (const m in msg) {

                        await telegramApi.send(message.chat.id, msg[m],logId)

                    }
                    next()
                    return
                }
            }

            await telegramApi.send(message.chat.id, {
                text: "Saya tidak mengerti maksud anda, silahkan ulangi /start"
            },logId)
            next()
            return

        } catch (error) {
            console.log(error)
            next()
            return
        }
    }
}

module.exports = new TelegramMiddleware().run