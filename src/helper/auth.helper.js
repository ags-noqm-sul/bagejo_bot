const { user, logger } = require("../models/index")

class AuthHelper {
    async validate(msg) {
        let userId = msg.from.id
        let access = await user.findOne({ "where": { "tele_id": userId } })

        if (access) {

            let active = access.active
            access.last_access = new Date().toLocaleString()
            await access.save()
            return ( active == 1) ? true : false

        } else {
            await user.create({
                tele_id: msg.from.id,
                first_name: msg.from.first_name,
                username: msg.from.username,
                last_access: new Date().toLocaleString()
            })
            return false
        }

    }

    async logger(msg) {
        let log = await logger.create({
            tele_id: msg.from.id,
            username: msg.from.username,
            name: msg.from.first_name,
            message: JSON.stringify(msg)
        })
        console.log("data", log)
        console.log("id", log.id)
        return log.id
    }

    async loggerUpdate(id, msg) {
        let log = await logger.findByPk(id)
        let response = log.response

        response += " " +msg

        await logger.update(
            { response: msg },
            { where: { id} }
        )
    }
}

module.exports = new AuthHelper()