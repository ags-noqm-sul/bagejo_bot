module.exports = {
    menu: (state) => {

        return {
            text: "untuk bantuan, silahkan ketuk menu berikut : ",
            reply_markup: {
                inline_keyboard:
                    [
                        [
                            { text: 'SITE FINDER', callback_data: 'site_finder' },
                            { text: 'SITE REVENUE', callback_data: 'site_revenue' }
                        ],
                        [
                            { text: 'LAC-CI FINDER', callback_data: 'lacci_finder' },
                            { text: 'ENB FINDER', callback_data: 'enb_finder' }
                        ],
                        [
                            { text: 'SITE SEARCH', callback_data: 'site_search' },
                            { text: 'TRM METRO', callback_data: 'trm_metro' }
                        ]
                    ]
            }
        }
    },
    toSiteString: (t) => {
        let txt
        txt = `${t.SITEID} ${t.Site_Location} \n`;
        txt += `Alamat : \n`;
        txt += `${t.Alamat} \n`;
        txt += `${t.Kelurahan}, ${t.Kecamatan}, ${t.Kota_Kabupaten}, ${t.Propinsi} \n\n`;
        txt += `Location \n`;
        txt += `https://www.google.com/maps/search/?api=1&query={${t.Site_Lat}},{${t.Site_Long}} \n`;
        return txt
    },
    callBackTemplate: (key) => {
        switch (key) {
            case 'site_finder':
                resultData = "Ketik site [spasi] MDOXXX"
                break;
            case 'site_revenue':
                resultData = "Ketik revenue [spasi] MDOXXX"
                break;
            case 'lacci_finder':
                resultData = "Ketik lacci [spasi] xxxx-yyyyy"
                break;
            case 'enb_finder':
                resultData = "Ketik enb [spasi] 375XXX"
                break;
            case 'site_search':
                resultData = "Ketik search [spasi] malalayang"
                break;
            case 'trm_metro':
                resultData = "Ketik trm [spasi] mdo001"
                break;
            default:
                resultData = "Saya tidak mengerti maksud anda"
        }
        return resultData
    },
    toSiteList: (s) => {
        let txt = ""
        let i = 0

        s.forEach(e => {
            i++
            txt += `${i}. ${e.SITEID} ${e.Site_Location} \n`
        });

        return txt
    },
    toText: (s) => {
        let txt = ""
        for (const [key, value] of Object.entries(s)) {
            txt += `${key}  :  ${value} \n`
        }
        return txt
    }
};