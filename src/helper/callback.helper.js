class CallbackHelper {
    async response(key) {

        // console.log(msg.message.chat.id)

        // let chatId = msg.message.chat.id
        // let clbkId = msg.id
        // let key = msg.data

        let resultData

        switch (key) {
            case 'site_finder':
                resultData = "Ketik site [spasi] MDOXXX"
                break;
            case 'site_revenue':
                resultData = "Ketik revenue [spasi] MDOXXX"
                break;
            case 'lacci_finder':
                resultData = "Ketik lacci [spasi] xxxx-yyyyy"
                break;
            case 'enb_finder':
                resultData = "Ketik enb [spasi] 375XXX"
                break;
            case 'site_search':
                resultData = "Ketik search [spasi] malalayang"
                break;
            case 'trm_metro':
                resultData = "Ketik trm [spasi] mdo001"
                break;
            default:
                resultData = "Saya tidak mengerti maksud anda"
        }

        return resultData

    }
}

module.exports = new CallbackHelper()