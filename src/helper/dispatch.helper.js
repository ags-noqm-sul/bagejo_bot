class DispatchHelper {
    async route(def, state, controller) {

        //tes
        console.log("dispatch helper call")
        

        const msg = def.req.body.message;
        const sentMessage = msg.text;
        const key = sentMessage.split(" ")[0]

        if (key === state){
            def.next()
            controller(msg,def.res,def.next)
        }

    }
}

module.exports = new DispatchHelper()