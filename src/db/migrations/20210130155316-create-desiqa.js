'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('desiqas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      SITEID: {
        type: Sequelize.STRING
      },
      NEID: {
        type: Sequelize.STRING
      },
      NEID_Sec: {
        type: Sequelize.STRING
      },
      Site_Location: {
        type: Sequelize.STRING
      },
      BTS_Name: {
        type: Sequelize.STRING
      },
      Network: {
        type: Sequelize.STRING
      },
      Vendor: {
        type: Sequelize.STRING
      },
      LACCI: {
        type: Sequelize.STRING
      },
      BSC_RNC_Id: {
        type: Sequelize.STRING
      },
      BSC_RNC_name: {
        type: Sequelize.STRING
      },
      BTS_no: {
        type: Sequelize.STRING
      },
      BTS_Name_OSS: {
        type: Sequelize.STRING
      },
      Cell_Name_OSS: {
        type: Sequelize.STRING
      },
      Cell_Name_OSS_2: {
        type: Sequelize.STRING
      },
      Site: {
        type: Sequelize.STRING
      },
      BTS: {
        type: Sequelize.STRING
      },
      Type: {
        type: Sequelize.STRING
      },
      Sec_no: {
        type: Sequelize.STRING
      },
      Sec_id: {
        type: Sequelize.STRING
      },
      LAC: {
        type: Sequelize.STRING
      },
      CI: {
        type: Sequelize.STRING
      },
      Band: {
        type: Sequelize.STRING
      },
      Bandwith_LTE: {
        type: Sequelize.STRING
      },
      TRX: {
        type: Sequelize.STRING
      },
      BTS_Type: {
        type: Sequelize.STRING
      },
      PCI_SCR_BCCH: {
        type: Sequelize.STRING
      },
      NCC: {
        type: Sequelize.STRING
      },
      BCC: {
        type: Sequelize.STRING
      },
      CGI: {
        type: Sequelize.STRING
      },
      Remark_DB: {
        type: Sequelize.STRING
      },
      Site_Long: {
        type: Sequelize.STRING
      },
      Site_Lat: {
        type: Sequelize.STRING
      },
      Azimuth: {
        type: Sequelize.STRING
      },
      Cluster_Sales: {
        type: Sequelize.STRING
      },
      NSA: {
        type: Sequelize.STRING
      },
      RTPO: {
        type: Sequelize.STRING
      },
      Branch: {
        type: Sequelize.STRING
      },
      Sub_Branch: {
        type: Sequelize.STRING
      },
      MSS_MGW: {
        type: Sequelize.STRING
      },
      Micro_Macro: {
        type: Sequelize.STRING
      },
      Type_Site: {
        type: Sequelize.STRING
      },
      Power_Type: {
        type: Sequelize.STRING
      },
      Daya: {
        type: Sequelize.STRING
      },
      Transport_Type: {
        type: Sequelize.STRING
      },
      Bandwith: {
        type: Sequelize.STRING
      },
      Border_WBB: {
        type: Sequelize.STRING
      },
      Tinggi_Tower: {
        type: Sequelize.STRING
      },
      Tinggi_Antena: {
        type: Sequelize.STRING
      },
      Remark_Tinggi_Antena: {
        type: Sequelize.STRING
      },
      Tipe_Antena: {
        type: Sequelize.STRING
      },
      Tilting_Antena: {
        type: Sequelize.STRING
      },
      Last_Update_Antena: {
        type: Sequelize.STRING
      },
      OA_Date: {
        type: Sequelize.STRING
      },
      QC_Pass_Date: {
        type: Sequelize.STRING
      },
      Propinsi: {
        type: Sequelize.STRING
      },
      Kota_Kabupaten: {
        type: Sequelize.STRING
      },
      Kecamatan: {
        type: Sequelize.STRING
      },
      Kelurahan: {
        type: Sequelize.STRING
      },
      Alamat: {
        type: Sequelize.TEXT
      },
      Tower_Provider: {
        type: Sequelize.STRING
      },
      Last_Update: {
        type: Sequelize.STRING
      },
      Keterangan: {
        type: Sequelize.STRING
      },
      Unvalidate_Date: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('desiqas');
  }
};