'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('transports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ID_SITE: {
        type: Sequelize.STRING
      },
      DATEL: {
        type: Sequelize.STRING
      },
      STO: {
        type: Sequelize.STRING
      },
      NAME_SITE: {
        type: Sequelize.STRING
      },
      NAME_METRO: {
        type: Sequelize.STRING
      },
      NAME_GPON: {
        type: Sequelize.STRING
      },
      PORT_GPON: {
        type: Sequelize.STRING
      },
      TIPE_ONT: {
        type: Sequelize.STRING
      },
      SN_ONT: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('transports');
  }
};