require('dotenv').config();
const axios = require('axios');
const url = 'https://api.telegram.org/bot';
const apiToken = process.env.APITOKENTELEGRAM;
const auth = require("../helper/auth.helper")
const Api = axios.create({
    baseURL: `${url}${apiToken}/`,
});

class telegramApi {
    async send(chatId, message, loggerId) {

        if (loggerId !== 0)
            auth.loggerUpdate(loggerId, message.text)

        Api.post('sendMessage',
            { ...message, chat_id: chatId }
        ).catch((error) => {
            console.log(error)
        });
    }

    async sendCallback(callBackId) {

        Api.post('answerCallbackQuery',
            {
                callback_query_id: callBackId,
                show_alert: 'true',
                parse_mode: 'html',
                text: ''
            }
        ).catch((error) => {
            console.log(error)
        });


    }
}

module.exports = new telegramApi()