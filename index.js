// Dependencies
const express = require('express');
const env = require('dotenv');
env.config();

const app = express();
const port = process.env.PORT;

const bodyParser = require('body-parser');
app.use(bodyParser.json());

let telegramMiddleware = require("./src/middleware/telegram.middleware")

app.post('/', telegramMiddleware,(req,res)=>{
    res.status(200).send("reveived")
});

let publicController = require("./src/logic/bot/public")
app.get('/store/:tankid/:volume', publicController.saveTank);

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});